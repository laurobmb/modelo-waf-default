# comandos 

    curl --user elastic:changeme http://elasticsearch:9200/xpack

    docker-compose build [container] or [all]
    
    docker-compose up -d [container] or [all]

    docker-compose stop [container] or [all]

    docker-compose rm [container] or [all]

    docker-compose logs [container] or [all] 

    rsync --delete -avzh -e "ssh -i ssh_key/janusec-master_key.pem" ELK/ janu@10.1.0.14:~/ELK 