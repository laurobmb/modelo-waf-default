# Proxy reverse Nginx with mod_security 

![nginx](../Imagens/NGINX.jpg)

### Build project
	podman build -t waf:latest -f ProxyReverse/Dockerfile

### POD command example
	podman run -it --rm -e BACKEND=181.215.183.135 -v $PWD/data/:/data/nginx/cache/projeto_waf/:Z --name waf -p80:8080 -p443:8443 quay.io/lagomes/waf:main

### Run project on daemon
	podman run --restart=always -d -e BACKEND=181.215.183.135 -v $PWD/data/:/data/nginx/cache/projeto_waf/:Z --name waf -p80:8080 -p443:8443 quay.io/lagomes/waf:main

### resource limits	
	podman run -it --rm --cpus=0.5 -m 512M --name waf -p80:8080 -p443:8443 quay.io/lagomes/waf:main
	 
#### Test
    http://DOMINIO/?exec=/bin/bash
    http://DOMINIO/?q="><script>alert(1)</script>"
    http://DOMINIO/?id=3 or 'a'='a'



