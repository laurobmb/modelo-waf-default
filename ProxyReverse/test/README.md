# Teste Locust

### Install
  pip install -r requirements.txt

### Execute
  locust -f test_garrucho.py --host https://robert.garrucho.com.br --users 500 --spawn-rate 20

### Access 
  http://127.0.0.1:8089
