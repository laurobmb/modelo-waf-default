# Ansible


## TAGs disponiveis:
- deploy_modsec
- copy_files_modsec
- install_filebeat
- install_docker
- install_logstash
- install_metricbeat
- time

## Comando instalar docker
    ansible-playbook -i inventory modsecurity.yaml --tags install_docker -l maquina

    ansible-playbook -i inventory modsecurity.yaml --tags install_docker -l modsec-prd

    ansible-playbook -i inventory modsecurity.yaml --tags install_docker -l modsec-hml

### Test
    ansible-playbook -i inventory modsecurity.yaml --tags install_docker -l ModsecAzureTeste

## Comando copy files modsec
    ansible-playbook -i inventory modsecurity.yaml --tags copy_files_modsec -l modsec

### Test
    ansible-playbook -i inventory modsecurity.yaml --tags copy_files_modsec -l ModsecAzureTeste

## Comando deploy container
    ansible-playbook -i inventory modsecurity.yaml --tags deploy_modsec -l modsec

### Test
    ansible-playbook -i inventory modsecurity.yaml --tags deploy_modsec -l ModsecAzureTeste

## Comando install filebeat e metricbeat
    ansible-playbook -i inventory modsecurity.yaml --tags install_metricbeat -l modsec
    
    ansible-playbook -i inventory modsecurity.yaml --tags install_filebeat -l modsec

### Test
    ansible-playbook -i inventory modsecurity.yaml --tags install_metricbeat -l ModsecAzureTeste
    
    ansible-playbook -i inventory modsecurity.yaml --tags install_filebeat -l ModsecAzureTeste

# Ativar nginx no metribeat e filebeat
    ansible -i inventory  -m shell -a "filebeat modules enable nginx" modsec
    
    ansible -i inventory  -m shell -a "metricbeat modules enable nginx" modsec

# Check health machines
## teste acesso
    ansible -i inventory -m ping modsec

## verifica Docker
    ansible -i inventory -m shell -a "docker ps" modsec

## Verifica status dos containers
    ansible -i inventory -m shell -a "docker stats --no-stream" modsec

# Copy files + deploy modsec
    ansible-playbook -i inventory modsecurity.yaml --tags copy_files_modsec,deploy_modsec -l maquina

    ansible-playbook -i inventory modsecurity.yaml --tags copy_files_modsec,deploy_modsec -l modsec-prd
    
    ansible-playbook -i inventory modsecurity.yaml --tags copy_files_modsec,deploy_modsec -l modsec-hml

# check cache
    ansible -i inventory -m shell -a "docker exec -it waf ls /data/nginx/cache" modsec

