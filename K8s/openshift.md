# Openshift

Comandos para deploy no openshift v4.6, a sequencia de comandos cria dois projetos distintos para testar a administracao de recursos da plataforma

## Projeto 1
    oc new-project waf
    oc new-app --name web-application-firewall --labels=app=waf --env=BACKEND=181.215.183.135 quay.io/lagomes/waf:main
    oc create serviceaccount waf
    oc adm policy add-scc-to-user anyuid -z waf
    oc set serviceaccount deployment/web-application-firewall waf
    oc create route passthrough web-application-firewall-tls --port 8443-tcp --service web-application-firewall --hostname conectarecife.recife.pe.gov.br
    oc set resources deployment web-application-firewall --limits=cpu=400m,memory=512Mi --requests=cpu=100m,memory=256Mi
    oc set probe deployment/web-application-firewall --readiness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck 
    oc set probe deployment/web-application-firewall --liveness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck
    oc autoscale deployment web-application-firewall --max 50 --min 3 --cpu-percent=80

## Projeto 2
    oc new-project waf-2
    oc new-app --name web-application-firewall-2 --labels=app=waf-2 --env=BACKEND=201.182.53.24 quay.io/lagomes/waf:main
    oc create serviceaccount waf-2
    oc adm policy add-scc-to-user anyuid -z waf-2
    oc set serviceaccount deployment/web-application-firewall-2 waf-2
    oc create route passthrough web-application-firewall-tls-2 --port 8443-tcp --service web-application-firewall-2 --hostname www. trf5.jus.br
    oc set resources deployment web-application-firewall-2 --limits=cpu=400m,memory=512Mi --requests=cpu=100m,memory=256Mi
    oc set probe deployment/web-application-firewall-2 --readiness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck 
    oc set probe deployment/web-application-firewall-2 --liveness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck
    oc autoscale deployment web-application-firewall-2 --max 50 --min 3 --cpu-percent=80

## Testes

Para testar a plataforma usei o DNSmasq para mudar as consultas de DNS da minha maquina de testes, e encaminhei os pacotes para os routers default do openshift exemplo de rota criada [router.yaml](route.yaml)

