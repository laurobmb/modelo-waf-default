# Install k8s

https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough

* az login
* az group create --name Decript --locations eastus
* az aks create --name WAF --resource-group Decript --node-count 4 --enable-addons monitoring --generate-ssh-keys
* az aks get-credentials --name WAF --resource-group Decript 
* az aks install-cli

# Install ingress NGINX

https://docs.microsoft.com/en-us/azure/aks/ingress-tls

helm install nginx-ingress ingress-nginx/ingress-nginx \
    --version 4.0.13 \
    --namespace ingress-basic --create-namespace \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."kubernetes\.io/os"=linux \
    --set controller.admissionWebhooks.patch.nodeSelector."kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."kubernetes\.io/os"=linux \
    --set defaultBackend.image.digest=""
