#!/bin/bash

SITES=(
    google.com
)

for i in ${SITES[*]}; do
    echo -n "check $i -> "
    curl -s -o /dev/null --insecure -w "%{http_code}" -I https://$i
    echo " "
done 