# usage

    podman run -p 8089:8089 -v $PWD/Test/locust/:/mnt/locust:Z locustio/locust -f /mnt/locust/locustfile.py --host https://www.trf5.jus.br --users 500 --spawn-rate 20 --web-host=0.0.0.0


# Distributed 

## Master
    podman run -p 8089:8089 -v $PWD/Test/locust/:/mnt/locust:Z locustio/locust -f /mnt/locust/locustfile.py --host https://www.trf5.jus.br --users 500 --spawn-rate 20 --master --web-host=0.0.0.0
## Worker
    podman run -v $PWD/Test/locust/:/mnt/locust:Z locustio/locust -f /mnt/locust/locustfile.py --worker --master-host=127.0.0.1   
